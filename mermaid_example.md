```mermaid
graph TD;
Start --> Ok;
Start --> Fail;
```

```mermaid
sequenceDiagram
    participant Alice
    participant David
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```


## Code Blocks

JSON:

```json
{
  "message": "Thanks for assisting to the Python Web Conference",
  "date": "2022-03-01",
  "success": true,
  "year": 2022
}
```

Python:

```python
import json

print(json.loads("file.json"))
```
