#!/usr/bin/env python3

# Python cells example

# You separate cells with #%%

#%%
print ("This is one cell")

#%% [markdown]

# # This is a Markdown cell

# You can have __markdown__

#%%
print ("Another cell")
